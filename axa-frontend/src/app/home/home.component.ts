﻿import { Component, OnInit, ViewChild } from '@angular/core';

import { User } from '../_models';
import { UserService } from '../_services/user.service';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute } from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';

@Component({
    templateUrl: 'home.component.html',
    styleUrls: ['home.component.css'],
})
export class HomeComponent implements OnInit {

    ngOnInit(): void {

    }
    users: User[] = [];
    currentUser: User;
    dataSource: MatTableDataSource<User>;

    displayedColumns: string[] = ['id', 'name', 'email', 'role', 'policy'];

    selectedValue: string = "Name";

    filters = [
        { value: 'Name' },
        { value: 'Id' },
    ];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private userService: UserService, private router: Router, private snackBar: MatSnackBar, private activeRoute: ActivatedRoute) {

        this.LoadAllClients(activeRoute.snapshot.queryParams);
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    LoadAllClients(params) {

        if (params.id === undefined) {
            this.userService.getAll().pipe(first()).subscribe(users => {
                this.dataSource = new MatTableDataSource(users);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            });
        } else {
            this.userService.getById(params.id).pipe(first()).subscribe(users => {
                console.log(users)
                this.users = [
                    users
                ]
                this.dataSource = new MatTableDataSource(this.users);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            });
        }
    }

    applyFilter(filterValue: string) {
        if (this.selectedValue === 'Name') {
            this.dataSource.filterPredicate = (data: User, filter: string) => data.name.trim().toLowerCase().indexOf(filter) !== -1;
        } else {
            this.dataSource.filterPredicate = (data: User, filter: string) => data.id.trim().toLowerCase().indexOf(filter) !== -1;
        }

        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

    goToPolicyMenu() {
        if (this.currentUser.role === 'user') {
            this.snackBar.open('You have not right', "Ok", {
                duration: 2000,
            });
        } else {
            this.router.navigate(['/policy']);
        }

    }

    goToPolicy(id: string) {
        if (this.currentUser.role === 'user') {
            this.snackBar.open('You have not right', "Ok", {
                duration: 2000,
            });
        } else {
            this.router.navigate(['/policy'], { queryParams: { id: id } });
        }
    }
}

