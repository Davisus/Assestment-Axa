﻿import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { AuthenticationService } from '../_services';
import {MatSnackBar} from '@angular/material';
import { HttpErrorResponse } from '@angular/common/http';
import { first } from 'rxjs/operators';
@Component({templateUrl: 'login.component.html'})
export class LoginComponent implements OnInit {
    loginForm: FormGroup;
    loading = false;
    submitted = false;
    returnUrl: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private snackBar: MatSnackBar) {}

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', Validators.required],
        });

        // reset login status
        this.authenticationService.logout();

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
    }

    // convenience getter for easy access to form fields
    get f() { return this.loginForm.controls; }

    onSubmit() {
        this.submitted = true;

        // stop here if form is invalid
        if (this.loginForm.invalid) {
            return;
        }

        this.loading = true;

        this.authenticationService.userAuthentication(this.f.username.value,'123').subscribe((data : any)=>{
            console.log(data.access_token);
            console.log(data.role);
            localStorage.setItem('userToken',data.access_token);
            localStorage.setItem('userRoles',data.role);

            this.authenticationService.login(this.f.username.value)
            .pipe(first())
            .subscribe(
                data => {
                    this.router.navigate([this.returnUrl]);
                },
                error => {
                   this.snackBar.open(error, "Ok", {
                        duration: 2000,
                      });
                    //this.alertService.error(error);
                    this.loading = false;
               });
          },
          (err : HttpErrorResponse)=>{
          });


    }
    
}