﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from '../../environments/environment';
@Injectable()
export class AuthenticationService {
    constructor(private http: HttpClient) { }

    userAuthentication(userName, password) {
        var data = "username=" + userName + "&password=" + password + "&grant_type=password";
        var reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-urlencoded','No-Auth':'True' });
        console.log(data);
        return this.http.post('http://localhost:35257/token', data, { headers: reqHeader });
      }

    login(username: string) {
        var reqHeader = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded'});
        
        return this.http.get<any>(`${environment.apiUrl}/clients?email=`+username).pipe(map((
            user=> {
                if (user) {
                    localStorage.setItem('currentUser', JSON.stringify(user));
                }else{
                    throw new Error('email not found');
                }
                return user;
            }
        )));
    }
    logout() {
        // remove user from local storage to log user out
        localStorage.removeItem('userToken');
        localStorage.removeItem('userRoles');
        localStorage.removeItem('currentUser');
    }
}