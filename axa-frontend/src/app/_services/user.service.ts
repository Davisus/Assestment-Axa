﻿import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../_models';
import { environment } from '../../environments/environment';
@Injectable()
export class UserService {
    constructor(private http: HttpClient) { }

    getAll(){
        var reqHeader = new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('userToken') });
        return this.http.get<User[]>(environment.apiUrl+'/clients',{headers:reqHeader});
    }

    getById(id: number) {
        var reqHeader = new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('userToken') });
        return this.http.get<User>(`${environment.apiUrl}/clients/` + id,{headers:reqHeader});
    }

    getByName(name: string) {
        var reqHeader = new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('userToken') });
        return this.http.get(`${environment.apiUrl}/clients?=name` + name,{headers:reqHeader});
    }
  
}