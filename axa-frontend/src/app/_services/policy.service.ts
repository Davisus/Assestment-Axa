import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Policy } from '../_models/policy';
import { environment } from '../../environments/environment';
@Injectable()
export class PolicyService {
    constructor(private http: HttpClient) { }

    getAll(){
        var reqHeader = new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('userToken') });
        return this.http.get<Policy[]>(environment.apiUrl+'/policies',{headers:reqHeader});
    }

    getById(id: string) {
        var reqHeader = new HttpHeaders({ 'Authorization': 'Bearer ' + localStorage.getItem('userToken') });
        return this.http.get<Policy[]>(`${environment.apiUrl}/policies/` + id,{headers:reqHeader});
    }
  
}