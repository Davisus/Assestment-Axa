export interface Policy {
    id: string;
    amountInsured: number;
    email: string;    
    inceptionDate: Date;
    installmentPayment: boolean;
    clientId: string;
}