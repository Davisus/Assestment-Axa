import { Component, OnInit, ViewChild } from '@angular/core';


import { PolicyService } from '../_services';
import { first } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { User, Policy } from '../_models';

@Component({
    templateUrl: 'policies.component.html',
    styleUrls: ['policies.component.css'],
})
export class PoliciesComponent implements OnInit {

    ngOnInit(): void {

    }

    currentUser: User;
    dataSource: MatTableDataSource<Policy>;

    displayedColumns: string[] = ['id', 'amountInsured', 'email', 'inceptionDate', 'installmentPayment','clientId'];

    @ViewChild(MatPaginator) paginator: MatPaginator;
    @ViewChild(MatSort) sort: MatSort;

    constructor(private policyService: PolicyService, private activeRoute: ActivatedRoute, private router: Router) {        
        this.LoadAllPolicies(activeRoute.snapshot.queryParams);
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    }

    LoadAllPolicies(params) {
        if(params.id === undefined){
            this.policyService.getAll().pipe(first()).subscribe(policies => {
                this.dataSource = new MatTableDataSource(policies);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            });
        }else{
            this.policyService.getById(params.id).pipe(first()).subscribe(policies => {
                this.dataSource = new MatTableDataSource(policies);
                this.dataSource.paginator = this.paginator;
                this.dataSource.sort = this.sort;
            });
        }
    }

    goToClients(id:string){
        this.router.navigate([''], { queryParams: { id: id } });
    }

    applyFilter(filterValue: string) {
        this.dataSource.filterPredicate = (data: Policy, filter: string) => data.email.trim().toLowerCase().indexOf(filter) !== -1;
        this.dataSource.filter = filterValue.trim().toLowerCase();

        if (this.dataSource.paginator) {
            this.dataSource.paginator.firstPage();
        }
    }

}

