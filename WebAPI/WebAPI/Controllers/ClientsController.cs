﻿using WebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class ClientsController : ApiController
    {
        private string pageHtml = "http://www.mocky.io/v2/5808862710000087232b75ac";

        // GET api/Clients
        [Authorize(Roles = "admin,user")]
        public async Task<IEnumerable<Client>> Get()
        {

            IEnumerable<Client> clients = null;

            try
            {
                HttpClient http = new HttpClient();

                var uri = new Uri(string.Format(pageHtml, string.Empty));

                HttpResponseMessage response = await http.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();
                    
                    clients = JsonConvert.DeserializeObject<Clients>(content).clients;
                }

                return clients;
            }
            catch (WebException ex)
            {

            }

            return clients;
        }

        // GET api/GetClientById/5
        [Authorize(Roles = "admin,user")]
        public async Task<Client> GetClientById(string id)
        {

            Client client = null;

            try
            {
                IEnumerable<Client> clients = await Get();

                client = clients.FirstOrDefault(c => c.id.Equals(id));

                return client;
            }
            catch (WebException ex)
            {

            }

            return client;
        }


        // GET api/GetClientByEmail/ex@email.com
        [AllowAnonymous]
        public async Task<Client> GetClientByEmail(string email)
        {

            Client client = null;

            try
            {
                IEnumerable<Client> clients = await Get();

                client = clients.FirstOrDefault(c => c.email.Equals(email));

                return client;
            }
            catch (WebException ex)
            {

            }

            return client;
        }

        // GET api/GetClientByName/pepe
        [Authorize(Roles = "admin,user")]
        public async Task<IEnumerable<Client>> GetClientByName(string name)
        {

            IEnumerable<Client> clients = null;

            try
            {
                IEnumerable<Client> data = await Get();

                clients = data.Where(c => c.name.ToLower().Contains(name.ToLower()));

                return clients;
            }
            catch (WebException ex)
            {

            }

            return clients;
        }       
    }
}
