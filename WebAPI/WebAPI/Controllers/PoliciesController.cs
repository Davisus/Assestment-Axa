﻿using WebAPI.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebAPI.Controllers
{
    public class PoliciesController : ApiController
    {
        private string pageHtml = "http://www.mocky.io/v2/580891a4100000e8242b75c5";

        // GET api/Policies
        [Authorize(Roles = "admin")]        
        public async Task<IEnumerable<Policy>> Get()
        {

            IEnumerable<Policy> Policies = null;

            try
            {
                HttpClient http = new HttpClient();

                var uri = new Uri(string.Format(pageHtml, string.Empty));

                HttpResponseMessage response = await http.GetAsync(uri);

                if (response.IsSuccessStatusCode)
                {
                    var content = await response.Content.ReadAsStringAsync();

                    Policies = JsonConvert.DeserializeObject<Policies>(content).policies;
                }

                return Policies;
            }
            catch (WebException ex)
            {

            }

            return Policies;
        }



        // GET api/Policies/5
        [Authorize(Roles = "admin")]
        public async Task<IEnumerable<Policy>> GetPolicyById(string id)
        {

            IEnumerable<Policy> policies = null;

            try
            {
                IEnumerable<Policy> clients = await Get();

                policies = clients.Where(c => c.clientId.Equals(id));

                return policies;
            }
            catch (WebException ex)
            {

            }

            return policies;
        }
    }
}
